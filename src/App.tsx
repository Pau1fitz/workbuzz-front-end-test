import { useEffect, useState } from "react";
import {
  Box,
  Container,
  CssBaseline,
  Grid,
  ThemeProvider,
} from "@mui/material";
import { theme } from "./theme/theme";
import { colors } from "./theme/colors";
import Header from "./components/Header/Header";
import { BodyText, HeaderText, SubHeaderText } from "./components/Typography";
import Button from "./components/Button/Button";
import Card from "./components/Card/Card";
import { fetchCharactersData, fetchComicsData } from "./utils";
import { ComicResponse } from "./types";
import Characters from "./components/Characters/Characters";
import BookmarkIcon from "@mui/icons-material/Bookmark";
import ReadingList from "./components/ReadingList/ReadingList";

const App = () => {
  const [isReadingListOpen, setIsReadingListOpen] = useState(false);
  const [offset, setOffset] = useState(1);
  const [comics, setComics] = useState([]);
  const [characters, setCharacters] = useState([]);
  const [readingList, setReadingList] = useState<ComicResponse[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [selectedCharacterId, setSelectedCharacterId] =
    useState<number | undefined>();

  const decreaseOffset = () => {
    if (offset >= 2) {
      setOffset(offset - 1);
    }
  };

  const increaseOffset = () => {
    if (comics.length === 4) {
      setOffset(offset + 1);
    }
  };

  useEffect(() => {
    const fetchComics = async () => {
      setIsLoading(true);
      const response = await fetchComicsData(offset, selectedCharacterId);

      setComics(response.data.results);
      setIsLoading(false);
    };

    fetchComics();
  }, [offset, selectedCharacterId]);

  useEffect(() => {
    const fetchCharacters = async () => {
      const response = await fetchCharactersData();
      setCharacters(response.data.results);
    };

    fetchCharacters();
  }, []);

  const addToReadingList = (item: ComicResponse) => {
    setReadingList([...readingList, item]);
  };

  const removeFromReadingList = (itemId: number) => {
    const updatedReadingList = readingList.filter((item) => {
      return item.id !== itemId;
    });

    setReadingList(updatedReadingList);
  };

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />

      <Header />

      <Container>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <HeaderText m="48px 0 24px" color={colors.white}>
            Comics
          </HeaderText>
          <Box
            onClick={() => setIsReadingListOpen(true)}
            sx={{ display: "flex" }}
          >
            <BookmarkIcon />
            <BodyText style={{ cursor: "pointer" }}>
              Reading List ({readingList.length})
            </BodyText>
          </Box>
        </Box>

        {isReadingListOpen && readingList.length > 0 && (
          <ReadingList
            setIsReadingListOpen={setIsReadingListOpen}
            removeFromReadingList={removeFromReadingList}
            readingList={readingList}
          />
        )}

        {isLoading && <SubHeaderText>Loading...</SubHeaderText>}

        {!isLoading && (
          <Characters
            characters={characters}
            setSelectedCharacterId={setSelectedCharacterId}
          />
        )}

        {!isLoading && comics.length === 0 ? (
          <SubHeaderText>No results</SubHeaderText>
        ) : (
          <Grid container spacing={6}>
            {comics.map((comic: ComicResponse) => (
              <Card
                key={comic.id}
                comic={comic}
                addToReadingList={addToReadingList}
              />
            ))}
          </Grid>
        )}

        <Box
          justifyContent="center"
          m="60px 0"
          sx={{ display: "flex", gap: "8px" }}
        >
          <Button onClick={decreaseOffset} text="Prev" />
          <Button onClick={increaseOffset} text="Next" />
        </Box>
      </Container>
    </ThemeProvider>
  );
};

export default App;
