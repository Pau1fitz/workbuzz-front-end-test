import md5 from "blueimp-md5";

const PUBLIC_KEY = "9aca44fc562c764d02ed6c0ec641e044";
const PRIVATE_KEY = "884fc439890af973d75698764746137430ebd294";

export const fetchComicsData = async (
  offset: number,
  selectedCharacterId?: number
) => {
  const ts = Date.now();
  const hash = md5(`${PUBLIC_KEY}${PRIVATE_KEY}${ts}`);
  const response = await fetch(
    `https://gateway.marvel.com/v1/public/characters/${
      selectedCharacterId ?? 1011334
    }/comics?limit=4&apikey=${PUBLIC_KEY}&hash=${hash}&offset=${offset * 4}`
  );

  if (!response.ok) {
    const errorData = await response.json();

    throw new Error(`Something went wrong: ${errorData}`);
  }

  return response.json();
};

export const fetchCharactersData = async () => {
  const ts = Date.now();
  const hash = md5(`${PUBLIC_KEY}${PRIVATE_KEY}${ts}`);
  const response = await fetch(
    `https://gateway.marvel.com/v1/public/characters?limit=5&apikey=${PUBLIC_KEY}&hash=${hash}`
  );

  if (!response.ok) {
    const errorData = await response.json();

    throw new Error(`Something went wrong: ${errorData}`);
  }

  return response.json();
};

type DateFormatOptions = Intl.DateTimeFormatOptions & {
  month: "long";
  day: "numeric";
  year: "numeric";
};

export const formatDate = (dateString: string) => {
  const date = new Date(dateString);
  const options: DateFormatOptions = {
    month: "long",
    day: "numeric",
    year: "numeric",
  };
  const formattedDate = date.toLocaleDateString("en-US", options);

  return formattedDate !== "Invalid Date" ? formattedDate : "-";
};
