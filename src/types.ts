export interface ComicResponse {
  id: number;
  digitalId: number;
  title: string;
  issueNumber: number;
  variantDescription: string;
  description: string | null;
  modified: string;
  isbn: string;
  upc: string;
  diamondCode: string;
  ean: string;
  issn: string;
  format: string;
  pageCount: number;
  textObjects: any[]; // change to the proper type if needed
  resourceURI: string;
  urls: { type: string; url: string }[];
  series: { resourceURI: string; name: string };
  variants: { resourceURI: string; name: string }[];
  collections: any[]; // change to the proper type if needed
  collectedIssues: any[]; // change to the proper type if needed
  dates: { type: string; date: string }[];
  prices: { type: string; price: number }[];
  thumbnail: { path: string; extension: string };
  images: any[]; // change to the proper type if needed
  creators: {
    available: number;
    collectionURI: string;
    items: { resourceURI: string; name: string; role: string }[];
    returned: number;
  };
  characters: {
    available: number;
    collectionURI: string;
    items: any[]; // change to the proper type if needed
    returned: number;
  };
  stories: {
    available: number;
    collectionURI: string;
    items: { resourceURI: string; name: string; type: string }[];
    returned: number;
  };
  events: {
    available: number;
    collectionURI: string;
    items: any[]; // change to the proper type if needed
    returned: number;
  };
}

export interface CharacterResponse {
  id: number;
  name: string;
  description: string;
  modified: string;
  thumbnail: {
    path: string;
    extension: string;
  };
  resourceURI: string;
  comics: {
    available: number;
    collectionURI: string;
    items: {
      resourceURI: string;
      name: string;
    }[];
    returned: number;
  };
  series: {
    available: number;
    collectionURI: string;
    items: {
      resourceURI: string;
      name: string;
    }[];
    returned: number;
  };
  stories: {
    available: number;
    collectionURI: string;
    items: {
      resourceURI: string;
      name: string;
      type: string;
    }[];
    returned: number;
  };
}
