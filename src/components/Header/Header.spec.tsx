import { render } from "@testing-library/react";
import "@testing-library/jest-dom";
import Header from "./Header";

it("renders the logo image", () => {
  const { getByAltText } = render(<Header />);
  const logoImage = getByAltText("logo");
  expect(logoImage).toBeInTheDocument();
});
