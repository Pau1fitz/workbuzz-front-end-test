import { Box, Container } from "@mui/material";
import image from "../../assets/logo.svg";
import { colors } from "../../theme/colors";

const Header = () => {
  return (
    <header>
      <Box
        sx={{
          borderBottom: `1px solid ${colors.neutral30}`,
          display: "flex",
          paddingTop: "8px",
        }}
        justifyContent="center"
      >
        <img src={image} alt="logo" />
      </Box>
    </header>
  );
};

export default Header;
