import {
  Button as MaterialUiButton,
  ButtonProps as MaterialUiButtonProps,
} from "@mui/material";
import { colors } from "../../theme/colors";

type ButtonProps = MaterialUiButtonProps & {
  text: string;
  onClick: () => void;
};

const Button: React.FC<ButtonProps> = ({ text, onClick, ...props }) => {
  return (
    <MaterialUiButton
      {...props}
      sx={{ backgroundColor: colors.neutral30 }}
      variant="contained"
      onClick={onClick}
    >
      {text}
    </MaterialUiButton>
  );
};

export default Button;
