import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import Button from "./Button";

describe("Button", () => {
  it("renders the correct text", () => {
    const text = "Click me!";
    render(<Button onClick={() => {}} text={text} />);
    expect(screen.getByText(text)).toBeInTheDocument();
  });
});
