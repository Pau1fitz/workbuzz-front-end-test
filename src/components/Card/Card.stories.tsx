import { ComponentStory, ComponentMeta } from "@storybook/react";
import { data } from "../../json";

import Card from "./Card";

export default {
  title: "Example/Card",
  component: Card,
} as ComponentMeta<typeof Card>;

const Template: ComponentStory<typeof Card> = (args) => (
  <Card comic={data[0]} />
);

export const Default = Template.bind({});
