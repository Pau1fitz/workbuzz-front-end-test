import { render } from "@testing-library/react";
import "@testing-library/jest-dom";
import { data } from "../../json";
import Card from "./Card";

describe("Card component", () => {
  it("renders the comic title", () => {
    const { getByText } = render(<Card comic={data[0]} />);
    const titleElement = getByText(/Mock Comic/);
    expect(titleElement).toBeInTheDocument();
  });

  it("renders the published date", () => {
    const { getByText } = render(<Card comic={data[0]} />);
    const dateElement = getByText(/October 7, 2019/);
    expect(dateElement).toBeInTheDocument();
  });

  it("renders the creator name", () => {
    const { getByText } = render(<Card comic={data[0]} />);
    const creatorElement = getByText(/Mock Creator/);
    expect(creatorElement).toBeInTheDocument();
  });

  it("renders the comic description", () => {
    const { getByText } = render(<Card comic={data[0]} />);
    const descriptionElement = getByText(/Mock description/);
    expect(descriptionElement).toBeInTheDocument();
  });
});
