import { Box, Grid } from "@mui/material";
import Button from "../Button/Button";
import { colors } from "../../theme/colors";
import { ComicResponse } from "../../types";
import { formatDate } from "../../utils";
import {
  BodyText,
  BodyTwoText,
  SubHeaderText,
  SubTitleText,
} from "../Typography";

type CardProps = {
  comic: ComicResponse;
  addToReadingList: (item: ComicResponse) => void;
};

const Card = ({ comic, addToReadingList }: CardProps) => {
  return (
    <Grid item sm={6} xs={12} container spacing={3}>
      <Grid item sm={6} xs={12}>
        <Box sx={{ position: "relative" }}>
          <img
            src={`${comic.thumbnail.path}.${comic.thumbnail.extension}`}
            style={{ width: "100%" }}
          />
          <Button
            text="Add"
            style={{
              position: "absolute",
              bottom: "20px",
              left: "50%",
              transform: "translate(-50%, -50%)",
            }}
            onClick={() => {
              addToReadingList(comic);
            }}
          />
        </Box>
      </Grid>
      <Grid item sm={6} xs={12} style={{ paddingLeft: "24px" }}>
        <SubHeaderText minHeight="86px">{comic.title}</SubHeaderText>

        <Box m="24px 0">
          <SubTitleText>Published:</SubTitleText>
          <BodyText>{formatDate(comic.dates[1].date)}</BodyText>
        </Box>

        <SubTitleText>Writer:</SubTitleText>
        <BodyText>
          {comic.creators.items[0] ? comic.creators.items[0].name : "-"}
        </BodyText>

        <BodyTwoText
          maxHeight="128px"
          sx={{ overflow: "hidden" }}
          color={colors.neutral70}
        >
          {comic.description ?? "-"}
        </BodyTwoText>
      </Grid>
    </Grid>
  );
};

export default Card;
