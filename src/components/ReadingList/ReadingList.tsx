import { Box, Drawer } from "@mui/material";
import { colors } from "../../theme/colors";
import { ComicResponse } from "../../types";
import CloseIcon from "@mui/icons-material/Close";
import { SubTitleText, SubTitleTextTwo } from "../Typography";

type ReadingListProps = {
  readingList: ComicResponse[];
  setIsReadingListOpen: (bool: boolean) => void;
  removeFromReadingList: (itemId: number) => void;
};

const ReadingList = ({
  readingList,
  setIsReadingListOpen,
  removeFromReadingList,
}: ReadingListProps) => {
  return (
    <div>
      <Drawer anchor={"right"} open={true} onClose={() => {}}>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            borderBottom: `1px solid ${colors.neutral10}`,
            padding: "24px 16px",
            minWidth: "250px",
          }}
        >
          <SubTitleText>Reading List</SubTitleText>
          <CloseIcon
            onClick={() => {
              setIsReadingListOpen(false);
            }}
            style={{ cursor: "pointer" }}
          />
        </Box>
        {readingList.map((item) => {
          return (
            <Box key={item.id}>
              <Box
                sx={{
                  display: "flex",
                  gap: "8px",
                  padding: "24px 16px",
                  alignItems: "center",
                }}
              >
                <img
                  src={`${item.thumbnail.path}.${item.thumbnail.extension}`}
                  style={{ width: "32px", height: "50px" }}
                />
                <SubTitleTextTwo sx={{ maxWidth: "142px" }}>
                  {item.title}
                </SubTitleTextTwo>
                <CloseIcon
                  onClick={() => {
                    removeFromReadingList(item.id);
                  }}
                  style={{ cursor: "pointer" }}
                />
              </Box>
            </Box>
          );
        })}
      </Drawer>
    </div>
  );
};

export default ReadingList;
