import { ComponentStory, ComponentMeta } from "@storybook/react";
import {
  HeaderText,
  SubHeaderText,
  SubTitleText,
  BodyText,
  BodyTwoText,
} from ".";

export default {
  title: "Example/Typography",
} as ComponentMeta<typeof HeaderText>;

const Template: ComponentStory<typeof HeaderText> = (args) => (
  <HeaderText {...args}>Text</HeaderText>
);

export const Header = Template.bind({});

export const SubHeader = Template.bind({});
SubHeader.args = {
  variant: "h3",
  fontSize: "31px",
  lineHeight: "43px",
  fontFamily: "Roboto Condensed",
};

export const SubTitle = Template.bind({});
SubTitle.args = {
  variant: "subtitle1",
  fontSize: "18px",
  lineHeight: "25px",
  fontFamily: "Roboto Condensed",
};

export const Body = Template.bind({});
Body.args = {
  variant: "body1",
  fontSize: "18px",
  lineHeight: "25px",
  fontWeight: 400,
};

export const BodyTwo = Template.bind({});
BodyTwo.args = {
  variant: "body2",
  fontSize: "14px",
  lineHeight: "21px",
  fontWeight: 400,
};
