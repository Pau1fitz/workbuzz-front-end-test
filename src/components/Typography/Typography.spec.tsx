import { render } from "@testing-library/react";
import "@testing-library/jest-dom";
import { HeaderText } from ".";

describe("HeaderText", () => {
  it("renders the correct text", () => {
    const text = "Hello, world!";
    const { getByText } = render(<HeaderText>{text}</HeaderText>);
    expect(getByText(text)).toBeInTheDocument();
  });
});
