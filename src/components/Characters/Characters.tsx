import { Box } from "@mui/material";
import { colors } from "../../theme/colors";
import { CharacterResponse, ComicResponse } from "../../types";
import { BodyText, SubTitleText } from "../Typography";

type CharactersProps = {
  characters: CharacterResponse[];
  setSelectedCharacterId: (id: number | undefined) => void;
};

const Characters = ({
  characters,
  setSelectedCharacterId,
}: CharactersProps) => {
  return (
    <Box
      sx={{
        background: colors.neutral30,
        borderRadius: "16px",
        padding: "24px",
        marginBottom: "40px",
      }}
    >
      <SubTitleText mb="8px" color={colors.neutral70}>
        Filter by character:
      </SubTitleText>
      <Box
        sx={{
          gap: "24px",
          display: "flex",
        }}
      >
        {characters.map((character) => {
          return (
            <BodyText
              key={character.id}
              sx={{ cursor: "pointer" }}
              onClick={() => {
                setSelectedCharacterId(character.id);
              }}
            >
              {character.name}
            </BodyText>
          );
        })}
      </Box>
    </Box>
  );
};

export default Characters;
