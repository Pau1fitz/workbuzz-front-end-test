export const data = [
  {
    id: 82967,
    digitalId: 0,
    title: "Mock Comic",
    issueNumber: 0,
    variantDescription: "",
    description: "Mock description",
    modified: "2019-11-07T08:46:15-0500",
    isbn: "",
    upc: "75960608839302811",
    diamondCode: "",
    ean: "",
    issn: "",
    format: "",
    pageCount: 112,
    textObjects: [],
    resourceURI: "http://gateway.marvel.com/v1/public/comics/82967",
    urls: [
      {
        type: "detail",
        url: "http://marvel.com/comics/issue/82967/marvel_previews_2017?utm_campaign=apiRef&utm_source=9aca44fc562c764d02ed6c0ec641e044",
      },
    ],
    series: {
      resourceURI: "http://gateway.marvel.com/v1/public/series/23665",
      name: "Marvel Previews (2017 - Present)",
    },
    variants: [
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/82965",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/82970",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/82969",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/74697",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/72736",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/75668",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/65364",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/65158",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/65028",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/75662",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/74320",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/73776",
        name: "Marvel Previews (2017)",
      },
    ],
    collections: [],
    collectedIssues: [],
    dates: [
      {
        type: "onsaleDate",
        date: "2099-10-30T00:00:00-0500",
      },
      {
        type: "focDate",
        date: "2019-10-07T00:00:00-0400",
      },
    ],
    prices: [
      {
        type: "printPrice",
        price: 0,
      },
    ],
    thumbnail: {
      path: "http://i.annihil.us/u/prod/marvel/i/mg/c/10/58dd01dbc6e51",
      extension: "jpg",
    },
    images: [
      {
        path: "http://i.annihil.us/u/prod/marvel/i/mg/c/10/58dd01dbc6e51",
        extension: "jpg",
      },
      {
        path: "http://i.annihil.us/u/prod/marvel/i/mg/5/80/4c361ae117d12",
        extension: "jpg",
      },
    ],
    creators: {
      available: 1,
      collectionURI:
        "http://gateway.marvel.com/v1/public/comics/82967/creators",
      items: [
        {
          resourceURI: "http://gateway.marvel.com/v1/public/creators/10021",
          name: "Mock Creator",
          role: "editor",
        },
      ],
      returned: 1,
    },
    characters: {
      available: 0,
      collectionURI:
        "http://gateway.marvel.com/v1/public/comics/82967/characters",
      items: [],
      returned: 0,
    },
    stories: {
      available: 2,
      collectionURI: "http://gateway.marvel.com/v1/public/comics/82967/stories",
      items: [
        {
          resourceURI: "http://gateway.marvel.com/v1/public/stories/183698",
          name: "cover from Marvel Previews (2017)",
          type: "cover",
        },
        {
          resourceURI: "http://gateway.marvel.com/v1/public/stories/183699",
          name: "story from Marvel Previews (2017)",
          type: "interiorStory",
        },
      ],
      returned: 2,
    },
    events: {
      available: 0,
      collectionURI: "http://gateway.marvel.com/v1/public/comics/82967/events",
      items: [],
      returned: 0,
    },
  },
  {
    id: 82965,
    digitalId: 0,
    title: "Marvel Previews (2017)",
    issueNumber: 0,
    variantDescription: "",
    description: null,
    modified: "2019-08-21T17:11:27-0400",
    isbn: "",
    upc: "75960608839302611",
    diamondCode: "JUL190068",
    ean: "",
    issn: "",
    format: "",
    pageCount: 152,
    textObjects: [],
    resourceURI: "http://gateway.marvel.com/v1/public/comics/82965",
    urls: [
      {
        type: "detail",
        url: "http://marvel.com/comics/issue/82965/marvel_previews_2017?utm_campaign=apiRef&utm_source=9aca44fc562c764d02ed6c0ec641e044",
      },
    ],
    series: {
      resourceURI: "http://gateway.marvel.com/v1/public/series/23665",
      name: "Marvel Previews (2017 - Present)",
    },
    variants: [
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/82967",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/82970",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/82969",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/74697",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/72736",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/75668",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/65364",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/65158",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/65028",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/75662",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/74320",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/73776",
        name: "Marvel Previews (2017)",
      },
    ],
    collections: [],
    collectedIssues: [],
    dates: [
      {
        type: "onsaleDate",
        date: "2099-08-28T00:00:00-0500",
      },
      {
        type: "focDate",
        date: "2019-08-05T00:00:00-0400",
      },
    ],
    prices: [
      {
        type: "printPrice",
        price: 0,
      },
    ],
    thumbnail: {
      path: "http://i.annihil.us/u/prod/marvel/i/mg/c/10/58dd01dbc6e51",
      extension: "jpg",
    },
    images: [
      {
        path: "http://i.annihil.us/u/prod/marvel/i/mg/c/10/58dd01dbc6e51",
        extension: "jpg",
      },
      {
        path: "http://i.annihil.us/u/prod/marvel/i/mg/5/80/4c361ae117d12",
        extension: "jpg",
      },
    ],
    creators: {
      available: 0,
      collectionURI:
        "http://gateway.marvel.com/v1/public/comics/82965/creators",
      items: [],
      returned: 0,
    },
    characters: {
      available: 0,
      collectionURI:
        "http://gateway.marvel.com/v1/public/comics/82965/characters",
      items: [],
      returned: 0,
    },
    stories: {
      available: 0,
      collectionURI: "http://gateway.marvel.com/v1/public/comics/82965/stories",
      items: [],
      returned: 0,
    },
    events: {
      available: 0,
      collectionURI: "http://gateway.marvel.com/v1/public/comics/82965/events",
      items: [],
      returned: 0,
    },
  },
  {
    id: 82970,
    digitalId: 52952,
    title: "Marvel Previews (2017)",
    issueNumber: 0,
    variantDescription: "",
    description: "",
    modified: "2020-02-07T09:35:32-0500",
    isbn: "",
    upc: "75960608839303111",
    diamondCode: "",
    ean: "",
    issn: "",
    format: "",
    pageCount: 112,
    textObjects: [],
    resourceURI: "http://gateway.marvel.com/v1/public/comics/82970",
    urls: [
      {
        type: "detail",
        url: "http://marvel.com/comics/issue/82970/marvel_previews_2017?utm_campaign=apiRef&utm_source=9aca44fc562c764d02ed6c0ec641e044",
      },
      {
        type: "purchase",
        url: "http://comicstore.marvel.com/Marvel-Previews-0/digital-comic/52952?utm_campaign=apiRef&utm_source=9aca44fc562c764d02ed6c0ec641e044",
      },
      {
        type: "reader",
        url: "http://marvel.com/digitalcomics/view.htm?iid=52952&utm_campaign=apiRef&utm_source=9aca44fc562c764d02ed6c0ec641e044",
      },
      {
        type: "inAppLink",
        url: "https://applink.marvel.com/issue/52952?utm_campaign=apiRef&utm_source=9aca44fc562c764d02ed6c0ec641e044",
      },
    ],
    series: {
      resourceURI: "http://gateway.marvel.com/v1/public/series/23665",
      name: "Marvel Previews (2017 - Present)",
    },
    variants: [
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/82967",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/82965",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/82969",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/74697",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/72736",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/75668",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/65364",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/65158",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/65028",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/75662",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/74320",
        name: "Marvel Previews (2017)",
      },
      {
        resourceURI: "http://gateway.marvel.com/v1/public/comics/73776",
        name: "Marvel Previews (2017)",
      },
    ],
    collections: [],
    collectedIssues: [],
    dates: [
      {
        type: "onsaleDate",
        date: "2099-01-29T00:00:00-0500",
      },
      {
        type: "focDate",
        date: "2020-01-06T00:00:00-0500",
      },
      {
        type: "unlimitedDate",
        date: "2020-01-29T00:00:00-0500",
      },
      {
        type: "digitalPurchaseDate",
        date: "2020-01-29T00:00:00-0500",
      },
    ],
    prices: [
      {
        type: "printPrice",
        price: 0,
      },
      {
        type: "digitalPurchasePrice",
        price: 0,
      },
    ],
    thumbnail: {
      path: "http://i.annihil.us/u/prod/marvel/i/mg/c/80/5e3d7536c8ada",
      extension: "jpg",
    },
    images: [
      {
        path: "http://i.annihil.us/u/prod/marvel/i/mg/c/10/58dd01dbc6e51",
        extension: "jpg",
      },
      {
        path: "http://i.annihil.us/u/prod/marvel/i/mg/5/80/4c361ae117d12",
        extension: "jpg",
      },
    ],
    creators: {
      available: 1,
      collectionURI:
        "http://gateway.marvel.com/v1/public/comics/82970/creators",
      items: [
        {
          resourceURI: "http://gateway.marvel.com/v1/public/creators/10021",
          name: "Jim Nausedas",
          role: "editor",
        },
      ],
      returned: 1,
    },
    characters: {
      available: 0,
      collectionURI:
        "http://gateway.marvel.com/v1/public/comics/82970/characters",
      items: [],
      returned: 0,
    },
    stories: {
      available: 1,
      collectionURI: "http://gateway.marvel.com/v1/public/comics/82970/stories",
      items: [
        {
          resourceURI: "http://gateway.marvel.com/v1/public/stories/183704",
          name: "cover from Marvel Previews (2017)",
          type: "cover",
        },
      ],
      returned: 1,
    },
    events: {
      available: 0,
      collectionURI: "http://gateway.marvel.com/v1/public/comics/82970/events",
      items: [],
      returned: 0,
    },
  },
  {
    id: 323,
    digitalId: 0,
    title: "Ant-Man (2003) #2",
    issueNumber: 2,
    variantDescription: "",
    description:
      "Ant-Man digs deeper to find out who is leaking secret information that threatens our national security.\r\n32 pgs./PARENTAL ADVISORY...$2.99",
    modified: "-0001-11-30T00:00:00-0500",
    isbn: "",
    upc: "5960605396-01911",
    diamondCode: "",
    ean: "",
    issn: "",
    format: "Comic",
    pageCount: 0,
    textObjects: [
      {
        type: "issue_solicit_text",
        language: "en-us",
        text: "Ant-Man digs deeper to find out who is leaking secret information that threatens our national security.\r\n32 pgs./PARENTAL ADVISORY...$2.99",
      },
    ],
    resourceURI: "http://gateway.marvel.com/v1/public/comics/323",
    urls: [
      {
        type: "detail",
        url: "http://marvel.com/comics/issue/323/ant-man_2003_2?utm_campaign=apiRef&utm_source=9aca44fc562c764d02ed6c0ec641e044",
      },
    ],
    series: {
      resourceURI: "http://gateway.marvel.com/v1/public/series/551",
      name: "Ant-Man (2003 - 2004)",
    },
    variants: [],
    collections: [],
    collectedIssues: [],
    dates: [
      {
        type: "onsaleDate",
        date: "2029-12-31T00:00:00-0500",
      },
      {
        type: "focDate",
        date: "-0001-11-30T00:00:00-0500",
      },
    ],
    prices: [
      {
        type: "printPrice",
        price: 2.99,
      },
    ],
    thumbnail: {
      path: "http://i.annihil.us/u/prod/marvel/i/mg/f/20/4bc69f33cafc0",
      extension: "jpg",
    },
    images: [
      {
        path: "http://i.annihil.us/u/prod/marvel/i/mg/c/10/58dd01dbc6e51",
        extension: "jpg",
      },
      {
        path: "http://i.annihil.us/u/prod/marvel/i/mg/5/80/4c361ae117d12",
        extension: "jpg",
      },
    ],
    creators: {
      available: 2,
      collectionURI: "http://gateway.marvel.com/v1/public/comics/323/creators",
      items: [
        {
          resourceURI: "http://gateway.marvel.com/v1/public/creators/600",
          name: "Clayton Crain",
          role: "penciller (cover)",
        },
        {
          resourceURI: "http://gateway.marvel.com/v1/public/creators/344",
          name: "Daniel Way",
          role: "writer",
        },
      ],
      returned: 2,
    },
    characters: {
      available: 0,
      collectionURI:
        "http://gateway.marvel.com/v1/public/comics/323/characters",
      items: [],
      returned: 0,
    },
    stories: {
      available: 2,
      collectionURI: "http://gateway.marvel.com/v1/public/comics/323/stories",
      items: [
        {
          resourceURI: "http://gateway.marvel.com/v1/public/stories/1808",
          name: "Cover #1808",
          type: "cover",
        },
        {
          resourceURI: "http://gateway.marvel.com/v1/public/stories/1809",
          name: "Interior #1809",
          type: "interiorStory",
        },
      ],
      returned: 2,
    },
    events: {
      available: 0,
      collectionURI: "http://gateway.marvel.com/v1/public/comics/323/events",
      items: [],
      returned: 0,
    },
  },
];
