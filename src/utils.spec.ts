import { formatDate } from "./utils";

describe("formatDate", () => {
  it("should format a valid date string", () => {
    const dateString = "2019-10-07T00:00:00-0400";
    const expectedDate = "October 7, 2019";

    expect(formatDate(dateString)).toEqual(expectedDate);
  });

  it('should return "-" for an invalid date string', () => {
    const dateString = "not a valid date string";

    expect(formatDate(dateString)).toEqual("-");
  });
});
